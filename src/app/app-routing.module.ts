import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule, routerReducer } from '@ngrx/router-store';
import { OffersPageComponent } from './offers-page/offers-page/offers-page.component';

export const appRoutes: Routes = [
  { path: ':id', component: SearchPageComponent },
  { path: ':id/offers', component: OffersPageComponent },
  { path: ':id/offers/:id2', component: OffersPageComponent },
  { path: '', component: HomePageComponent},
  { path: '**', component: NotFoundPageComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { enableTracing: false }),
    StoreModule.forRoot({
      router: routerReducer,
    }),
    StoreRouterConnectingModule.forRoot()
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}