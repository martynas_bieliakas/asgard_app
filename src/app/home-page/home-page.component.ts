import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootStoreState, GamesStoreSelectors } from '../root-store';
import { Observable } from 'rxjs';
import { GameItem } from '../models/game-item.model';

@Component({
  selector: 'app-homepage',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  games$: Observable<GameItem[]>;

  constructor(private store$: Store<RootStoreState.State>) {}
  
  ngOnInit() {
    this.games$ = this.store$.select(
      GamesStoreSelectors.selectGames
    );
  }

}
