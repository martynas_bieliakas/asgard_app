import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page.component';
import { GameTileComponent } from './game-tile/game-tile.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    HomePageComponent,
    GameTileComponent
  ],
  exports: [HomePageComponent]
})
export class HomePageModule { }
