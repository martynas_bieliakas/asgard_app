
import { Injectable } from '@angular/core';
import { AsgardHttpClientService } from '../../http-clients/asgard-http-client.service';
import { GameItem } from '../../models/game-item.model';
import { map } from 'rxjs/operators';
import { Game } from '../../models/game.model';
import { GameAttribute } from '../../models/game-attribute.model';


@Injectable({
  providedIn: 'root',
})
export class GamesRetrievalService {
	url = 'games';
	constructor(private _http: AsgardHttpClientService) {}

	getGames() {
		return this._http.get(this.url).pipe(
      map(response => {
        return this.toGameType(response);
      })
    );
  }

  private toGameType(response: any): Game[] {
    return Object.keys(response).map(index => {
      let game = response[index];
      game.title = index;
      game.attributes = this.toAttributeType(game.attributes);
      game.items = this.toGameItemType(game.items)
      return game;
    });
  }

  private toAttributeType(attributes: GameAttribute[]) {
    return Object.keys(attributes).map(index => {
      let attribute = <GameAttribute> {
        title: index,
        values: attributes[index]
      };
      return attribute;
    });
  }

  private toGameItemType(items: GameItem[]) {
    return Object.keys(items).map(index => {
      let item = items[index];
      item.title = index;
      item.attributes = Object.keys(item.attributes).map(index => {
        let attribute = {};
        attribute[index] = item.attributes[index];
        return attribute;
      });
      return item;
    });
  }
}
