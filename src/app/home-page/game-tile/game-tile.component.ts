import { Component, OnInit, Input, HostListener } from '@angular/core';
import { GameItem } from '../../models/game-item.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game-tile',
  templateUrl: './game-tile.component.html',
  styleUrls: ['./game-tile.component.css']
})
export class GameTileComponent implements OnInit {

  @Input()
  game: GameItem;

  @HostListener('click')
  onClick() {
    this.navigateToSearchPage();
  }
  
  constructor(private router: Router) { }

  ngOnInit() {
  }
  
  private navigateToSearchPage() {
    let path = this.game.title.toLowerCase().replace(' ', '-');
    this.router.navigate([path]);
  }

}
