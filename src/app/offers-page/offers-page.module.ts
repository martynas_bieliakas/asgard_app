import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffersPageComponent } from './offers-page/offers-page.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [OffersPageComponent]
})
export class OffersPageModule { }
