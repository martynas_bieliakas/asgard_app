import { Component, OnInit } from '@angular/core';
import { GameItem } from '../../models/game-item.model';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RootStoreState, GamesStoreSelectors } from '../../root-store';
import { Store } from '@ngrx/store';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-offers-page',
  templateUrl: './offers-page.component.html',
  styleUrls: ['./offers-page.component.css']
})
export class OffersPageComponent implements OnInit {
  item$: Observable<GameItem>;

  constructor(private store$: Store<RootStoreState.State>,
    private route: ActivatedRoute) { }

  ngOnInit() {
    // this.item$ = this.route.params.pipe(
    //   switchMap(params =>
    //     this.store$.select(
    //       GamesStoreSelectors.selectGameItem(
    //         this.getGameTitle(params['id']),
    //         this.getItemTitle(params['id'])
    //       )
    //     ) as Observable<GameItem>
    //   )
    // );
    // this.item$.subscribe(game => {
    //   console.log(game);
    // });
  }

  private getGameTitle(routeParam: string) {
    return routeParam.split('-')[0];
  }

  private getItemTitle(routeParam: string) {
    return routeParam.split('-')[1];
  }

}
