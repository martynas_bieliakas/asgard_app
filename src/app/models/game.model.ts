import { GameAttribute } from "./game-attribute.model";
import { GameItem } from "./game-item.model";


export interface Game {
  title: string;
  image_url: string;
  attributes: GameAttribute[];
  items: GameItem[];
}
