export interface GameItem {
  title: string;
  image_url: string;
  attributes: object[];
}