export interface GameAttribute {
  title: string;
  values: string[];
}
