import { Component, OnInit } from '@angular/core';
import { GameItem } from '../models/game-item.model';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { RootStoreState, GamesStoreSelectors } from '../root-store';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { Router } from "@angular/router";
import { Game } from '../models/game.model';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {
  game$: Observable<Game>;
  gameTitle: string;
  filteredGameItems: GameItem[];
  filters: any;

  set gameItems(value: GameItem[]) {
    if (value && value.length > 0) {
      this._gameItems = value;
      this.updateProductList();
    }
  }

  get gameItems() {
    return this._gameItems;
  }

  private _gameItems: GameItem[];
  private _platform: string;

  constructor(private store$: Store<RootStoreState.State>,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.game$ = this.route.params.pipe(
      switchMap( params =>
        this.store$.select(
          GamesStoreSelectors.selectGame(params['id'])
        ) as Observable<Game>
      )
    );
    this.game$
      .subscribe(game => {
        this.gameItems = game.items;
        this.gameTitle = game.title;
      }
    );
  }

  updateFilter(filter: any) {
    this.filters = filter;
    this.updateProductList();
  }

  updatePlatform(platform: string) {
    this._platform = platform;
  }

  selectGameItem(gameItem: GameItem) {
    let route = this.createItemUrl(gameItem);
    this.router.navigate([route]);
  }

  private updateProductList() {
    this.filteredGameItems = this.getFilterdGameItemList();
  }

  private getFilterdGameItemList() {
    return this.gameItems.filter(item => {
      return item.attributes.some(itemAttr => !!_.find(this.filters, itemAttr))
    });
  }

  private createItemUrl(gameItem: GameItem) {
    let itemTitle = gameItem.title.replace(' ', '').toLowerCase();
    let gameTitle = this.gameTitle.replace(' ', '').toLowerCase();
    return `${itemTitle}-${gameTitle}-${this._platform.toLowerCase()}/offers`;
  }
}
