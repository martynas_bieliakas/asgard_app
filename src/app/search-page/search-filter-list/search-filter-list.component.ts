import { Component,  Input, EventEmitter, Output, OnInit } from '@angular/core';
import { QuestionBase } from '../../dynamic-form/question-base';
import { DropdownQuestion } from '../../dynamic-form/question-dropdown';
import { Game } from '../../models/game.model';
import { GameAttribute } from '../../models/game-attribute.model';


@Component({
  selector: 'app-search-filter-list',
  templateUrl: './search-filter-list.component.html',
  styleUrls: ['./search-filter-list.component.css']
})
export class SearchFilterListComponent implements OnInit {
  @Input()
  set game(value: Game) {
    if (value.attributes) {
      this.createFilterQuestions(value.attributes);
    }
  }

  @Output()
  filtersChanged = new EventEmitter<any>();
  @Output()
  platformChanged = new EventEmitter<string>();

  set selectedPlatform(value: string) {
    this._platform = value;
    this.platformChanged.emit(value);
  };
  get selectedPlatform() {
    return this._platform;
  }
  questions: QuestionBase<any>[] = [];
  platforms = ['PC', 'Xbox', 'PS4'];

  private _platform: string;

  ngOnInit() {
    this.selectedPlatform = 'PS4';
  }

  onFilterChange(filterValues: any) {
    this.filtersChanged.emit(filterValues);
  }

  private createFilterQuestions(attributes: GameAttribute[]) {
    attributes.forEach(attr => {
      let options = [];
      attr.values.forEach(v =>
        options.push({ key: v, value: v})
      );
      this.questions.push(
        new DropdownQuestion({
          key: attr.title,
          label: attr.title,
          options
        })
      );
    });
  }

}
