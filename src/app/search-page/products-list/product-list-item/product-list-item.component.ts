import { Component, Input, Output, EventEmitter } from '@angular/core';
import { GameItem } from '../../../models/game-item.model';

@Component({
  selector: 'app-product-list-item',
  templateUrl: './product-list-item.component.html',
  styleUrls: ['./product-list-item.component.css']
})
export class ProductListItemComponent {
  @Input()
  item: GameItem;

  @Output()
  onProductSelect = new EventEmitter<GameItem>();

  itemSelected() {
    this.onProductSelect.emit(this.item);
  }
}
