import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GameItem } from '../../models/game-item.model';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent {

  @Input()
  items: GameItem[];

  @Output()
  onSelectedGameItem = new EventEmitter<GameItem>();

  productSelected(gameItem: GameItem) {
    this.onSelectedGameItem.emit(gameItem);
  }

}
