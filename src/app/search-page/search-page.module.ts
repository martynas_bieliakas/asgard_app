import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchPageComponent } from './search-page.component';
import { SearchFilterListComponent } from './search-filter-list/search-filter-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductsListComponent } from './products-list/products-list.component';
import { DynamicFormModule } from '../dynamic-form/dynamic-form.module';
import { ProductListItemComponent } from './products-list/product-list-item/product-list-item.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    DynamicFormModule,
    FormsModule
  ],
  declarations: [
    SearchPageComponent,
    SearchFilterListComponent,
    ProductsListComponent,
    ProductListItemComponent
  ],
  exports: [SearchPageComponent]
})
export class SearchPageModule { }
