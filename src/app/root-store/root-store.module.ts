import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GamesStoreModule } from './games-store/games-store.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  imports: [
    CommonModule,
    GamesStoreModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      name: 'Asgard App'
    }),
  ],
  declarations: []
})
export class RootStoreModule { }
