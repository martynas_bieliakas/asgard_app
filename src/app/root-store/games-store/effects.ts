import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of as observableOf } from 'rxjs';
import { catchError, map, tap, switchMap } from 'rxjs/operators';
import { GamesRetrievalService } from '../../home-page/services/games-retrieval.service';

import * as gamesActions from './actions';


@Injectable()
export class GamesStoreEffects {
  constructor(private _gamesRetrievalService: GamesRetrievalService, private actions$: Actions) {}

  @Effect()
  loadGamesEffect$: Observable<Action> = this.actions$.pipe(
    ofType<gamesActions.LoadGamesAction>(
      gamesActions.ActionTypes.LOAD_GAMES
    ),
    switchMap(action =>
      this._gamesRetrievalService
      .getGames()
      .pipe(
        map(
          games => 
            new gamesActions.SetGamesAction({games})
        ),
        catchError(error =>
          observableOf(new gamesActions.GamesLoadErrorAction({ error }))
        )
      )
    )
  );

  // @Effect()
  // chooseGame$: Observable<Action> = this.actions$.pipe(
  //   ofType<gamesActions.SelectGameItemAction>(
  //     gamesActions.ActionTypes.SELECT_GAME_ITEM
  //   ),
  //   switchMap(action =>
  //     this._gamesRetrievalService
  //     .getGames()
  //     .pipe(
  //       map(
  //         games => 
  //           new gamesActions.SetGamesAction({games})
  //       ),
  //       catchError(error =>
  //         observableOf(new gamesActions.GamesLoadErrorAction({ error }))
  //       )
  //     )
  //   )
  // );
}