import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { featureReducer } from './reducer';
import { GamesStoreEffects } from './effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('games', featureReducer),
    EffectsModule.forFeature([GamesStoreEffects])
  ],
  providers: [GamesStoreEffects]
})
export class GamesStoreModule { }
