import { createFeatureSelector,createSelector, 
  MemoizedSelector } from '@ngrx/store';
import { State } from './state';
import { Game } from '../../models/game.model';

const getError = (state: State): any => state.error;

const getIsLoading = (state: State): boolean => state.isLoading;

const getGames = (state: State): any => state.games;


export const selectGamesState: MemoizedSelector<
  object,
  State
> = createFeatureSelector<State>('games');

export const selectGamesError: MemoizedSelector<object, any> = createSelector(
  selectGamesState,
  getError
);

export const selectGamesIsLoading: MemoizedSelector<
  object,
  boolean
> = createSelector(selectGamesState, getIsLoading);

export const selectGames: MemoizedSelector<
  object,
  Game[]
> = createSelector(selectGamesState, getGames);

export const selectGame = (name: string) => createSelector(selectGamesState, getGames, gamesState => {
  if (gamesState.games) {
    return gamesState.games.find(game => {
      return game.title.toLowerCase().replace(' ', '-') === name;
    });
  } else {
    return {};
  }
});