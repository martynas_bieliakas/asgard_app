
import { Actions, ActionTypes } from './actions';
import { initialState, State } from './state';

export function featureReducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.LOAD_GAMES:
      return {
        ...state,
        error: null,
        isLoading: true
      };
    case ActionTypes.SET_GAMES:
      return {
        ...state,
        games: action.payload.games,
        error: null,
        isLoading: false,

      };
    case ActionTypes.LOAD_ERROR:
      return {
        ...state,
        error: action.payload.error,
        isLoading: false
      };
    default: {
        return state;
    }
  }
 }