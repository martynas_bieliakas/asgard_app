import { Game } from "../../models/game.model";

export interface State {
  games: Game[] | null;
  isLoading: boolean;
  error: string;
}

export const initialState: State = {
  games: null,
  isLoading: false,
  error: null
}