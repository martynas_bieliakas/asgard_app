import { Action } from '@ngrx/store';
import { Game } from '../../models/game.model';


export enum ActionTypes {
  LOAD_GAMES = '[Games] Load Games',
  SET_GAMES = '[Games] Set Games',
  LOAD_ERROR = '[Games] Loading Error',
}

export class LoadGamesAction implements Action {
  readonly type = ActionTypes.LOAD_GAMES;
  constructor() {}
}

export class GamesLoadErrorAction implements Action {
  readonly type = ActionTypes.LOAD_ERROR;
  constructor(public payload: {error: string}) {}
}

export class SetGamesAction implements Action {
  readonly type = ActionTypes.SET_GAMES;
  constructor(public payload: { games: Game[] }) {}
}

export type Actions = 
  LoadGamesAction |
  GamesLoadErrorAction |
  SetGamesAction;