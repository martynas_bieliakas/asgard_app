import { GamesStoreState } from './games-store';

export interface State {
  gamesFeature: GamesStoreState.State;
}

