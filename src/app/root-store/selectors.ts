import { createSelector, MemoizedSelector } from '@ngrx/store';
import { GamesStoreSelectors } from './games-store';

export const selectError: MemoizedSelector<object, string> = createSelector(
  GamesStoreSelectors.selectGamesError,
  (gameStoreError: string) => {
    return gameStoreError;
  }
);

export const selectIsLoading: MemoizedSelector<
  object,
  boolean
> = createSelector(
  GamesStoreSelectors.selectGamesIsLoading,
  (gamesStoreLoading: boolean) => {
    return gamesStoreLoading;
  }
);