import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { GamesRetrievalService } from './home-page/services/games-retrieval.service';
import { AsgardHttpClientService } from './http-clients/asgard-http-client.service';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { AppRoutingModule } from './app-routing.module';
import { SearchPageModule } from './search-page/search-page.module';
import { HomePageModule } from './home-page/home-page.module';
import { RootStoreModule } from './root-store';
import { OffersPageModule } from './offers-page/offers-page.module';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SearchPageModule,
    HomePageModule,
    RootStoreModule,
    OffersPageModule
  ],
  providers: [
    GamesRetrievalService,
    AsgardHttpClientService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
