import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GameItem } from '../models/game-item.model';

@Injectable({
  providedIn: 'root'
})
export class AsgardHttpClientService {

  baseUrl = 'http://localhost:3000/';
  constructor(private _http: HttpClient) {}

  get<T>(url: string, options = undefined) {
    let getUrl = this.baseUrl + url;
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this._http.get<T>(getUrl, {headers});
  }
}
