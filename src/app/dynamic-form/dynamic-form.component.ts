import { Component, Input, OnInit, Output, EventEmitter }  from '@angular/core';
import { FormGroup }                 from '@angular/forms';
 
import { QuestionBase }              from './question-base';
import { QuestionControlService }    from './question-control.service';
 
@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  providers: [ QuestionControlService ]
})
export class DynamicFormComponent implements OnInit {
 
  @Input() questions: QuestionBase<any>[] = [];
  form: FormGroup;

  @Output()
  values = new EventEmitter<any>();

  payLoad = '';
 
  constructor(private qcs: QuestionControlService) {  }
 
  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.questions);
  }
 
  onSelectChange() {
    this.values.emit(this.toObjectArray(this.form.value));
  }

  private toObjectArray(value: any) {
    return Object.keys(value).map(index => {
      let a = {};
      a[index] = value[index];
      return a;
    });
  }
}