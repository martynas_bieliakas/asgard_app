import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup }        from '@angular/forms';
 
import { QuestionBase }     from '../question-base';

 
@Component({
  selector: 'app-question',
  templateUrl: './dynamic-form-question.component.html',
  styleUrls: ['dynamic-form-question.component.css']
})
export class DynamicFormQuestionComponent {
  @Input()
  question: QuestionBase<any>;

  @Input()
  form: FormGroup;

  @Output()
  change = new EventEmitter();

  onChange() {
    this.change.emit();
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
}