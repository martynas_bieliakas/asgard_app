# AsgardMarketplaceApp

To start this app do these steps

1. In the console navigate to app root folder.
2. Run command 'npm install'.
3. After succesful install, run command 'npm run server'.
4. In a separate console instance in app root run command 'npm start'.
5. When the dev server starts, navigate in the browser to http://localhost:4200/.
